#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    intersectionType = Intersector2D2D::NoInteresection;
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    pointLine.setZero();
    tangentLine.setZero();

}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{

    matrixNomalVector.row(0) = planeNormal;
    rightHandSide(0) = planeTranslation;
    return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;
    rightHandSide(1) = planeTranslation;

  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    //default: no intersezione
    intersectionType = NoInteresection;
    //tangent line=crossprod(n1T and n2T
    tangentLine = Vector3d( matrixNomalVector(0, 1)*matrixNomalVector(1,2)
                            - matrixNomalVector(0,2)*matrixNomalVector(1,1) ,
                            -(matrixNomalVector(0,0)*matrixNomalVector(1,2)
                              - matrixNomalVector(0,2)*matrixNomalVector(1,0)),
                            matrixNomalVector(0,0)*matrixNomalVector(1,1)
                            -matrixNomalVector(0,1)*matrixNomalVector(1,0));
    //check= n1T*x1 - n2T*x2
    double check =  ( ( matrixNomalVector.row(0).transpose() )*rightHandSide(0) -
                     ( matrixNomalVector.row(1).transpose() )*rightHandSide(1) ).squaredNorm();
    //le due normali sono parallele:
    //i piani sono paralleli
    if(tangentLine.squaredNorm() <= toleranceParallelism){
        if(abs(check) <= toleranceIntersection){
            //i piani sono coincidenti
            intersectionType = Coplanar;
            return false;
        }else{
            //i piani non si intersecano
            intersectionType = NoInteresection;
            return false;
        }
    }else{
        //le normali e, quindi, i piani si intersecano
        intersectionType = LineIntersection;
        return true;
    }

    return false;
}
