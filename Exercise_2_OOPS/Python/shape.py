import math


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int = 0, b: int = 0):
        self.__center = center
        self.__a = a
        self.__b = b

    def area(self):

        return math.pi * self.__a * self.__b



class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        Ellipse.__init__(self, center, radius, radius)

    def area(self):
        return Ellipse.area(self)

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
    def area(self):
        return 0.5*abs(self._p1.x*self._p2.y + self._p2.x*self._p3.y +self._p3.x*self._p1.y -self._p2.x*self._p1.y - self._p3.x*self._p2.y -self._p1.x*self._p3.y)


class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge: int):
        P1: Point = Point(p1.x + edge, p1.y)
        P2: Point = p1
        P3: Point = Point(p1.x+edge / 2, p1.y + edge * math.sqrt(3)/2)
        Triangle.__init__(self, P1, P2, P3)

    def area(self):
        return Triangle.area(self)


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self._p1 = p1
        self._p2 = p2
        self._p3 = p3
        self._p4 = p4

    def area(self):
        return 0.5*abs(self._p1.x*self._p2.y + self._p2.x*self._p3.y +self._p3.x*self._p4.y + self._p4.x*self._p1.y -self._p2.x*self._p1.y - self._p3.x*self._p2.y -self._p4.x*self._p3.y -self._p1.x*self._p4.y)


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        Quadrilateral.__init__(self, p1, p2, Point(p2.x + (p4.x - p1.x),p4.y), p4)

    def area(self):
        return Quadrilateral.area(self)

class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        Parallelogram.__init__(self,p1,Point(p1.x + base, p1.y), Point(p1.x + base, p1.y + height))

    def area(self):
        return Parallelogram.area(self)




class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        Rectangle.__init__(self, p1, edge, edge)

    def area(self):
        return Rectangle.area(self)
