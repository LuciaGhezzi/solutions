#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1.0E-7;
    toleranceParallelism = 1.0E-7;
    intersectionParametricCoordinate =0;
    intersectionType=Intersector2D1D::NoInteresection;
}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal;
    planeTranslationPointer = &planeTranslation;
    return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
    return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    //default: no intersezione
    intersectionType = NoInteresection;
    //controllo se c'è parallelismo tra il piano e la retta:
    //z=dot_product(nT, t) : z==0 <=> nT e t perpendicolari
    double z = (*planeNormalPointer).transpose() * *lineTangentPointer;
    if(z >= toleranceParallelism){
        // retta e piano non sono paralleli:
        // tra il piano e la retta c'è intersezione
        double firstterm =( (*planeNormalPointer)*(*planeTranslationPointer ) ).squaredNorm();
        double secondterm = (*planeNormalPointer).transpose() * *lineOriginPointer;
        //calcolo la coordinata parametrica = (nT*x0 - nT*y0)/nTt
        intersectionParametricCoordinate = (firstterm - secondterm)/z;
        intersectionType = PointIntersection;
        IntersectionPoint();
        return true;
    }else{
        //normale e retta sono perpendicolari:
        //la retta e il piano sono paralleli
        //o la retta appartiene al piano
        //check = nT*y0
        double check = (*planeNormalPointer).transpose() * *lineOriginPointer;
        if(check <= toleranceIntersection){
            //la linea giace sul piano
            intersectionType = Coplanar;
            return false;
        }
    }
    //no intersection
    return false;
}
