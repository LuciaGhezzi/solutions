#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() { X=0; Y=0; }
      Point(const double& x,
            const double& y) {X=x; Y=y; }
      Point(const Point& point) { *this = point; }

      double ComputeNorm2() const;

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " << point.X <<" y ="<< point.Y;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs)
      {if(lhs.Perimeter()<rhs.Perimeter()) return true;
       else return false;
      }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs)
      {if(lhs.Perimeter()>rhs.Perimeter()) return true;
       else return false;
      }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs)
      {if(lhs.Perimeter()<=rhs.Perimeter()) return true;
       else return false;
      }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs)
      {if(lhs.Perimeter()>=rhs.Perimeter()) return true;
      else return false;
       }
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() {}
      Ellipse(const Point& center,
              const double& a,
              const double& b) { _center= center; _a=a; _b=b; }
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) {_center.X=point.X; _center.Y=point.Y; }
      void SetSemiAxisA(const double& a) {_a=a;}
      void SetSemiAxisB(const double& b) {_b=b;}
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle()  {}
      Circle(const Point& center,
             const double& radius) { _center = center; _a=radius; _b=radius;}
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle()
      {}
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point)
      {
          points.push_back(point);
          //if(points.size() > 4){throw runtime_error("too many/few points inserted");}
      }
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral(){}
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p)
      {
         points.push_back(p);
         //if(points.size() > 5){throw runtime_error("too many/few points inserted");}
      }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle() {}
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square() {}
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {}
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
