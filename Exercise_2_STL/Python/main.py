
class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.pizza_ingredients=[]

    def addIngredient(self, ingredient: Ingredient):
        self.pizza_ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.pizza_ingredients)

    def computePrice(self) -> int:
        price=0
        for i in range(0, self.numIngredients()):
            price = price + self.pizza_ingredients[i].Price
        return price

class Order:
    def __init__(self):
        self.pizza_list=[]

    def getPizza(self, position: int) -> Pizza:
        return self.pizza_list[position]

    def initializeOrder(self, numPizzas: int):
        self.pizza_list[numPizzas]=0

    def addPizza(self, pizza: Pizza):
        self.pizza_list.append(pizza)

    def numPizzas(self) -> int:
        return len(self.pizza_list)

    def computeTotal(self) -> int:
        tot_price =0
        for i in range(0, len(self.pizza_list)):
            tot:Pizza=self.pizza_list[i]
            tot_price += tot.computePrice()
        return tot_price


class Pizzeria:
    def __init__(self):
        self.ingredient_list= {}
        self.pizza_set={}
        self.order_set={}

    def addIngredient(self, name: str, description: str, price: int):
        if name in self.ingredient_list:
              raise Exception("Ingredient already inserted")
        self.ingredient_list[name]=Ingredient(name, price, description)

    def findIngredient(self, name: str) -> Ingredient:

         if name not in self.ingredient_list:
            raise Exception("Ingredient not found")

         return self.ingredient_list[name]

    def addPizza(self, name: str, ingredients: []):
        if name in self.pizza_set:
            raise Exception("Pizza already inserted")

        current_ing: Ingredient
        current_pizza=Pizza(name)
        current_pizza.Name=name
        for i in range(0, len(ingredients)):
            if ingredients[i] in self.ingredient_list:
                current_pizza.addIngredient(self.ingredient_list[ingredients[i]])
        self.pizza_set[name] = current_pizza

    def findPizza(self, name: str) -> Pizza:
        if name not in self.pizza_set:
            raise Exception("Pizza not found")
        return self.pizza_set[name]


    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")
        num_order=1000
        last_order=Order()
        for i in range(0, len(pizzas)):
            if pizzas[i] in self.pizza_set:
                last_order.addPizza(self.pizza_set[pizzas[i]])
        self.order_set[num_order] = last_order
        num_order+=1
        return num_order-1


    def findOrder(self, numOrder: int) -> Order:
        if numOrder not in self.order_set:
            raise Exception("Order not found")
        return self.order_set[numOrder]


    def getReceipt(self, numOrder: int) -> str:
        receipt:str=''
        if numOrder not in self.order_set:
            raise Exception("Order not found")
        current_ord:Order=self.order_set[numOrder]
        total = str(current_ord.computeTotal())
        numpizzas = current_ord.numPizzas()
        for i in range(0,numpizzas):
            current_pizza:Pizza=current_ord.getPizza(i)
            receipt += "- " + current_pizza.Name + ", "+str(current_pizza.computePrice()) + " euro\n"
        tot = str(receipt + "  TOTAL: " + total + " euro\n")
        return tot


    def listIngredients(self) -> str:
        provv_ing_list:str=''
        sortedIngredient = sorted(self.ingredient_list)

        for i in range(0, len(self.ingredient_list)):
            ingr=self.ingredient_list[sortedIngredient[i]]
            provv_ing_list += ingr.Name + " - '" + ingr.Description + "': " + str(ingr.Price) + " euro\n"
        return provv_ing_list

    def menu(self) -> str:
        stringa:str=''
        pizzaname = sorted(self.pizza_set)
        for i in range(0, len(self.pizza_set)):
            pizza=self.pizza_set[pizzaname[i]]
            stringa+= pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) + " euro\n"
        return stringa
