#define _USE_MATH_DEFINES
#include <cmath>
#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

    Point::Point(const double &x, const double &y)
    {
      _x = x;
       _y = y;
    }

    Point::Point(const Point &point)
    {
        _x = point._x;
        _y = point._y;
    }


    Ellipse::Ellipse(const Point &center, const int &a, const int &b)
    {
       _center = center;
       _a = a;
       _b = b;
    }

    double Ellipse::Area() const { return _a * _b * M_PI;}


    Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius)
    {}

    double Circle::Area() const
    {
        return Ellipse::Area();
    }

    Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
    {
        _p1._x = p1._x;
        _p1._y = p1._y;
        _p2._x = p2._x;
        _p2._y = p2._y;
        _p3._x = p3._x;
        _p3._y = p3._y;
    }

    double Triangle::Area() const
    {
        double area = 0.5*abs((_p1._x*_p2._y + _p2._x*_p3._y +(_p3._x*_p1._y) -_p2._x*_p1._y - _p3._x*_p2._y -_p1._x*_p3._y));
        return area;
    }

    TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(Point(p1._x+edge, p1._y), p1, Point(p1._x+edge*0.5,p1._y+ (edge*sqrt(3)*0.5) ))
    {}

    double TriangleEquilateral::Area() const
    {
        return Triangle::Area();
    }

    Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
    {
        _p1._x = p1._x;
        _p1._y = p1._y;
        _p2._x = p2._x;
        _p2._y = p2._y;
        _p3._x = p3._x;
        _p3._y = p3._y;
        _p4._x = p4._x;
        _p4._y = p4._y;
    }

    double Quadrilateral::Area() const
    {

        return 0.5*abs(_p1._x*_p2._y +_p2._x*_p3._y +_p3._x*_p4._y + _p4._x*_p1._y -_p2._x*_p1._y - _p3._x*_p2._y -_p4._x*_p3._y -_p1._x*_p4._y);
    }

    Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4) : Quadrilateral(p1, p2, Point(p2._x +(p4._x-p1._x), p4._y), p4)
    {}

    double Parallelogram::Area() const
    {
        return Quadrilateral::Area();
    }

    Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Parallelogram(p1,Point(p1._x + base, p1._y), Point(p1._x + base, p1._y + height))
    {}

    double Rectangle::Area() const
    {
        return Parallelogram::Area();
    }

    Square::Square(const Point &p1, const int &edge) : Rectangle(p1, edge, edge)
    {}

    double Square::Area() const
    {
        return Rectangle::Area();
    }






















}

