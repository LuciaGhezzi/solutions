*# Pizzeria

The manager of a pizzeria in London wants to create a pizza manager which is able to:

* manage the ingredients in the kitchen
* manage the pizza menu
* manage the receipts of the clients

## Requirements

Write a software which is able to manage the Pizzeria as required from the following class diagram:

![list](Images/pizza_cd.png)

The class `Pizzeria` is the base class from which we can:

1. add an ingredient to the list of ingredients:
    * the ingredient is identified by its name 
    * the ingredient contains also a brief description and a price in euro;
    * if the ingredient already exists in the pizzeria, an exception `"Ingredient already inserted"` is thrown
2. add a pizza in the menu:
    * the pizza is identified by its name; 
    * the list of ingredients is also passed during the creation;
    * from each pizza it is possible to take the total number of ingredients 
    * the price of the pizza is made by the sum of its ingredients
    * ingredient duplication is allowed ("_reinforcement_")
    * if the pizza already exists in the menu, an exception `"Pizza already inserted"` is thrown;
3. get an ingredient from the kitchen:
    * if the ingredient is not found an exception `"Ingredient not found"` is thrown;
4. get a pizza from the menu: 
    * if the pizza is not found an exception `"Pizza not found"` is thrown;
5. create an order for a client:
    * the list of pizzas in the order are passed during the order request
    * the orders is identified by a number `numOrder` which starts from `1000` and increments for each client
    * pizza duplication is allowed
    * empty order is not allowed and an exception `"Empty order"` is thrown
    * from the order it is possible to get the total number of pizzas and to get each pizza from the position starting from 1 to the total number of pizzas; if the position passed is wrong an exception `"Position passed is wrong"`;
    * total amount of the order is computed as the sum of all the pizzas contained
6. get an order: 
    * if the order is not found an exception `"Order not found"` is thrown;
7. print the total list of ingredients in the kitchen in alphabetical order
    * the list is a `string` with the following format:
    
        ```
          for each ingredient in the list
              name + " - '" + description + "': " + price + " euro" + "\n"
        ```
    
    __Example__:

        Mozzarella - 'Traditionally southern Italian cheese': 3 euro
        Tomato - 'Red berry of the plant Solanum lycopersicum': 2 euro
        

      
8. show the menu:
    * the menu is a `string` with the following format:
    
        ```
        for each pizza in the menu
            name + " (" + numIngredients + " ingredientsW): " + price + " euro" + "\n"
        ```
    
    
    __Example__:

        Margherita (2 ingredients): 5 euro
        Marinara (1 ingredients): 2 euro


      
9. get the receipt correspondent to an order
    * if the order is not found an exception `"Order not found"` is thrown;
    * the receipt is a `string` with the following format:
    
        ```
        for each pizza in the order
            "- " + name + ", " + price + " euro" + "\n"   
        "  TOTAL: " + total + " euro" + "\n"  
        ```   
    
    __Example__:
        
        - Margherita, 5 euro
        - Marinara, 2 euro
          TOTAL: 7 euro
        *