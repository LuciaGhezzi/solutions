#include "Pizzeria.h"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <unordered_set>


namespace PizzeriaLibrary {


Ingredient::Ingredient(const string &name, const int &price, const string &description)
{
    Name = name;
    Price = price;
    Description = description;
}

Pizza::Pizza(const string& name){ Name = name;}

void Pizza::AddIngredient(const Ingredient &ingredient) { PizzaIngredientList.push_back(ingredient); }

int Pizza::NumIngredients() const {return PizzaIngredientList.size();}

int Pizza::ComputePrice() const
{
    unsigned int price=0;
    for(list<Ingredient>::const_iterator it=PizzaIngredientList.cbegin(); it!=PizzaIngredientList.cend(); it++)
    {
       price = price + (*it).Price;
    }
    return price;
}



void Order::InitializeOrder(int numPizzas)
{
    if(numPizzas == 0){
        throw runtime_error("Empty order");
    }else{ PizzaVector.reserve(numPizzas);}
}

void Order::AddPizza(const Pizza &pizza)
{
    PizzaVector.push_back(pizza);
}

const Pizza &Order::GetPizza(const int &position) const
{
    return PizzaVector[position];
}

int Order::NumPizzas() const
{
    return PizzaVector.size();
}

int Order::ComputeTotal() const
{
    int totalprice=0;
    for (unsigned int i=0; i<NumPizzas(); i++){
        totalprice += PizzaVector[i].ComputePrice();
    }
    return totalprice;
}





void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{

    if(IngredientMap.find(name)!= IngredientMap.end())
    {
        throw runtime_error("Ingredient already inserted");
    }

    Ingredient lastIngredient = Ingredient(name, price, description);
    IngredientMap.insert(pair<string, Ingredient>(name, lastIngredient));

}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    if(IngredientMap.find(name) == IngredientMap.end())
    {throw runtime_error("Ingredient not found");}
    return(IngredientMap.find(name)->second);

}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    if(PizzaSet.find(name) != PizzaSet.end())
    { throw runtime_error("Pizza already inserted"); }

    Ingredient CurrentIng;
    Pizza pizzaSupp;
    pizzaSupp.Name=name;
    for(unsigned int i= 0; i < ingredients.size(); i++)
    {
        if(IngredientMap.find(ingredients[i]) != IngredientMap.end()){
            pizzaSupp.AddIngredient(IngredientMap.at(ingredients[i]));
           }
    }
           PizzaSet.insert(pair<string, Pizza>(name, pizzaSupp));
}


const Pizza &Pizzeria::FindPizza(const string &name) const
{
   if(PizzaSet.find(name) == PizzaSet.end()){
       throw runtime_error("Pizza not found");
   }
   return(PizzaSet.find(name)->second);

}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
   Order thisOrder;
   thisOrder.NumOrder = 0;
   thisOrder.InitializeOrder(pizzas.size());
   thisOrder.NumOrder = LastOrder.NumOrder + OrderSet.size() + 1;
   for(unsigned int i= 0; i< pizzas.size(); i++){
       thisOrder.AddPizza(thisOrder.GetPizza(i));
   }
   OrderSet.insert(pair<int, Order>(thisOrder.NumOrder, thisOrder));
   LastOrder = thisOrder;
   return LastOrder.NumOrder;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    if(OrderSet.find(numOrder) == OrderSet.end())
    {
        throw runtime_error("Order not found");
    }else{
         return (OrderSet.find(numOrder)->second);
    }
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt;
    if(numOrder > OrderSet.size()){
        throw runtime_error("Order not found");
    }else {
        for(unsigned int i=0; i< FindOrder(numOrder).PizzaVector.size(); i++)
        {
            receipt +=  "- " + FindOrder(numOrder).PizzaVector[i].Name + ", " + to_string(FindOrder(numOrder).PizzaVector[i].ComputePrice()) + " euro" ;
        }
     return receipt;
     string total;
     total += " TOTAL: " + to_string(FindOrder(numOrder).ComputeTotal()) + " euro";
     return total;
    }
 }


string Pizzeria::ListIngredients() const
{
    string listIngredients;
   for( auto& ingredient : IngredientMap){
       Ingredient Ing = ingredient.second;
       listIngredients += Ing.Name + " - '" + Ing.Description + "': " + to_string(Ing.Price)+ " euro\n" ;
   }
   return listIngredients;
}

string Pizzeria::Menu() const
{
    string menu;
    for(auto& pizza : PizzaSet){
        Pizza CurrentPizza = pizza.second;
        menu +=  CurrentPizza.Name + " (" + to_string(CurrentPizza.NumIngredients()) + " ingredients): " + to_string(CurrentPizza.ComputePrice()) + " euro\n";
    }
    return menu;
}




}
