#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt(( (_a*_a) + (_b*_b))*0.5);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.reserve(3);
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    for(unsigned int i=0; i<3; i++){
        perimeter += sqrt((points[i].X - points[(i+1)%3].X)*(points[i].X - points[(i+1)%3].X) + (points[i].Y - points[(i+1)%3].Y)*(points[i].Y - points[(i+1)%3].Y));
    }
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
      Point p2, p3;
      p2.X=p1.X+edge;
      p2.Y = p1.Y;
      p3.X = p1.X+(edge*0.5);
      p3.Y = p1.Y + edge*0.5*sqrt(3);
      points.clear();
      points.reserve(3);
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);

  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.reserve(4);
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    for(unsigned int i=0; i<4; i++){
        perimeter += sqrt((points[i].X - points[(i+1)%4].X)*(points[i].X - points[(i+1)%4].X) + (points[i].Y - points[(i+1)%4].Y)*(points[i].Y - points[(i+1)%4].Y));
    }
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
      Point p2;
      Point p3;
      Point p4;
      p2.X = p1.X;
      p2.Y = p1.Y + height;
      p3.X = p2.X + base;
      p3.Y = p2.Y;
      p4.X = p3.X;
      p4.Y = p1.Y;
      points.clear();
      points.reserve(4);
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  Square::Square(const Point &p1, const double &edge)
  {
      Point p2;
      Point p3;
      Point p4;
      p2.X = p1.X;
      p2.Y = p1.Y + edge;
      p3.X = p2.X + edge;
      p3.Y = p2.Y;
      p4.X = p3.X;
      p4.Y = p1.Y;
      points.clear();
      points.reserve(4);
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);

  }

  double Point::ComputeNorm2() const
  {
    return sqrt(X*X + Y*Y);
  }

  Point Point::operator+(const Point& point) const
  {
   return Point(X+point.X, Y+point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
    return Point(X-point.X, Y-point.Y);
  }

  Point&Point::operator-=(const Point& point)
  {
      X -= point.X;
      Y -= point.Y;
      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }


}
