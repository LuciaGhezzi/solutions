# include "viaMichelin.h"

#include <iostream>
#include <fstream>
#include <sstream>

namespace ViaMichelinLibrary {
int RoutePlanner::BusAverageSpeed = 50;

void BusStation::Load()
{
    _numberBuses = 0;
    _buses.clear();

    ifstream file;
    file.open(_busFilePath.c_str());

    if (file.fail())
        throw runtime_error ("Something goes wrong");

    try {
        string line;
        getline(file,line);
        getline(file,line);

        istringstream converter;
        converter.str(line);
        converter>> _numberBuses;

        getline(file,line);
        getline(file,line);

        _buses.reserve(_numberBuses);
        for(int b = 0; b < _numberBuses; b++)
        {
            int idBus,fuelCost;

            getline(file,line);
            istringstream converter;
            converter.str(line);
            converter>> idBus >> fuelCost;

            _buses.push_back( Bus ());
            Bus& bus = _buses[b];
            bus.Id = idBus;
            bus.FuelCost = fuelCost;

        }
        file.close();

    }  catch (exception) {
        _numberBuses = 0;
        _buses.clear();

        throw runtime_error ("Something goes wrong");
    }

}

const Bus &BusStation::GetBus(const int &idBus) const
{
    if( idBus > _numberBuses)
        throw runtime_error ("Bus " + to_string(idBus) + " does not exists");

    return _buses[idBus - 1];
}

void MapData::Load()
{
    ifstream file;
    file.open(_mapFilePath.c_str());

    if (file.fail())
        throw runtime_error ("Something goes wrong");

    try {
        string line;

        //Get BusStop
        getline(file,line);//salto commento
        getline(file,line);

        istringstream busStopsConverter;
        busStopsConverter.str(line);
        busStopsConverter>> _numberBusStops;

        getline(file,line);//salto commento
        _busStops.reserve(_numberBusStops);
        for(int b = 0; b < _numberBusStops; b++)
        {
            int idBusStop, latitude, longitude;
            string name;

            getline(file,line);
            istringstream busStopConverter;
            busStopConverter.str(line);
            busStopConverter>> idBusStop>> name>> latitude>> longitude;

            _busStops.push_back( BusStop());
            BusStop& busStop = _busStops[b];
            busStop.Id = idBusStop;
            busStop.Latitude = latitude;
            busStop.Longitude = longitude;
            busStop.Name = name;
        }

        //Get Steets
        getline(file,line);//salto commento
        getline(file,line);

        istringstream streetsConverter;
        streetsConverter.str(line);
        streetsConverter>> _numberStreets;

        getline(file,line);//salto commento
        _streets.reserve(_numberStreets);
        _streetFrom.reserve(_numberStreets);
        _streetTo.reserve(_numberStreets);
        for(int s = 0; s < _numberStreets; s++)
        {
            int idStreet, from, to, travelTime;

            getline(file,line);
            istringstream streetConverter;
            streetConverter.str(line);
            streetConverter>> idStreet>> from>> to>> travelTime;

            _streets.push_back( Street());
            Street& street = _streets[s];
            street.Id = idStreet;
            street.TravelTime = travelTime;

            _streetFrom.push_back(from);
            _streetTo.push_back(to);
        }
        //Get Routes
        getline(file,line);//salto commento
        getline(file,line);

        istringstream routesConverter;
        routesConverter.str(line);
        routesConverter>> _numberRoutes;

        getline(file,line);//salto commento
        _routes.reserve(_numberRoutes);
        _routeStreets.reserve(_numberRoutes);
        for(int r = 0; r < _numberRoutes; r++)
        {
            int idRoute, numberStreets;

            getline(file,line);
            istringstream streetConverter;
            streetConverter.str(line);
            streetConverter>> idRoute>> numberStreets;

            _routes.push_back( Route());
            Route& route = _routes[r];
            route.Id = idRoute;
            route.NumberStreets = numberStreets;

            _routeStreets.push_back(vector<int>());
            _routeStreets[r].reserve(numberStreets);
            for(int s = 0; s< numberStreets; s++)
            {
                int idStreet;
                streetConverter>> idStreet;

                _routeStreets[r].push_back(idStreet);
            }
        }


        file.close();

    }  catch (exception) {

        throw runtime_error ("Something goes wrong");
    }
}

const Street &MapData::GetRouteStreet(const int &idRoute, const int &streetPosition) const
{
    if( idRoute > _numberRoutes)
        throw runtime_error ("Route " + to_string(idRoute) + " does not exists");

    const vector<int>& streets = _routeStreets[idRoute -1];
    if( streetPosition >= (int)streets.size())
        throw runtime_error("Street at position " + to_string(streetPosition) + " does not exists");

    const int& idStreet = streets[streetPosition];
    return GetStreet(idStreet);
}

const Route &MapData::GetRoute(const int &idRoute) const
{
    if( idRoute > _numberRoutes)
        throw runtime_error ("Route " + to_string(idRoute) + " does not exists");

    return _routes[idRoute - 1];
}

const Street &MapData::GetStreet(const int &idStreet) const
{
    if( idStreet > _numberStreets)
        throw runtime_error ("Street " + to_string(idStreet) + " does not exists");

    return _streets[idStreet - 1];
}

const BusStop &MapData::GetStreetFrom(const int &idStreet) const
{
    if( idStreet > _numberStreets)
            throw runtime_error ("Street " + to_string(idStreet) + " does not exists");

        const int& idBusStop = _streetFrom[idStreet - 1];
        return GetBusStop(idBusStop);
}

const BusStop &MapData::GetStreetTo(const int &idStreet) const
{
    if( idStreet > _numberStreets)
        throw runtime_error ("Street " + to_string(idStreet) + " does not exists");

    const int& idBusStop = _streetTo[idStreet - 1];
    return GetBusStop(idBusStop);
}

const BusStop &MapData::GetBusStop(const int &idBusStop) const
{
    if( idBusStop > _numberBusStops)
        throw runtime_error ("BusStop " + to_string(idBusStop) + " does not exists");

    return _busStops[idBusStop - 1];
}

int RoutePlanner::ComputeRouteTravelTime(const int &idRoute) const
{
    int totalTravelTime = 0;

    const Route& route = _mapData.GetRoute(idRoute);
    const int& numStreets = route.NumberStreets;

    for( int s = 0; s < numStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        totalTravelTime += street.TravelTime;
    }
    return totalTravelTime;
}

int RoutePlanner::ComputeRouteCost(const int &idBus, const int &idRoute) const
{
    int totalTravelCost = 0;

    const Route& route = _mapData.GetRoute(idRoute);
    const Bus& bus = _busStation.GetBus(idBus);
    const int& numStreets = route.NumberStreets;

    for( int s = 0; s < numStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        totalTravelCost += street.TravelTime * bus.FuelCost * BusAverageSpeed;
    }
    return (totalTravelCost / 3600);
}

string MapViewer::ViewRoute(const int &idRoute) const
{
    const Route& route = _mapData.GetRoute(idRoute);

    ostringstream routeView;
    routeView<< route.Id << ": ";

    for(int s = 0; s < route.NumberStreets; s++)
    {
        const Street& street = _mapData.GetRouteStreet(idRoute, s);
        const BusStop& from = _mapData.GetStreetFrom(street.Id);
        routeView<< from.Name<< " -> ";

        if( s == route.NumberStreets - 1)
        {
            const BusStop& to = _mapData.GetStreetTo(street.Id);
            routeView<< to.Name;
        }
    }

    return routeView.str();
}

string MapViewer::ViewStreet(const int &idStreet) const
{
    const BusStop& from = _mapData.GetStreetFrom(idStreet);
    const BusStop& to = _mapData.GetStreetTo(idStreet);

    return to_string(idStreet) +
            ": " +
            from.Name +
            " -> " +
            to.Name;
}


string MapViewer::ViewBusStop(const int &idBusStop) const
{
    const BusStop& busStop = _mapData.GetBusStop(idBusStop);

    return busStop.Name +
            " (" +
            to_string((double)busStop.Latitude / 10000.0) +
            ", " +
            to_string((double)busStop.Longitude / 10000.0) +
            ")";
}


}
