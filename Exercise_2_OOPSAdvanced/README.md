# Polygon

The computation of polygon perimeter is a fundamental part of Computational Science.

Most interesting polygons are for example:

* Triangle
* Quadrilateral
* Ellipse

## Requirements

Write a software able to compute the perimeter of the polygons listed:

* Triangle
  * Triangle Equilateral
* Quadrilateral
  * Rectangle
  * Square
* Ellipse
  * Circle

The following structure shall be implemented:

![polygon_cd](Images/polygon_cd.png)
