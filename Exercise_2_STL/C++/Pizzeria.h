#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <map>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <queue>
//#include <unordered_set>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
      Ingredient(){Name=" "; Price = 0; Description =" ";}
      Ingredient(const string& name, const int& price, const string& description);
  };

  class Pizza {
    public:
      string Name;
      list<Ingredient> PizzaIngredientList;

      Pizza(){Name = " ";}
      Pizza(const string& name);
      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const ;
      int ComputePrice() const;
  };

  class Order {
    public:
      vector<Pizza> PizzaVector;
      Pizza LastPizza;
      unsigned int NumOrder;
      Order(){NumOrder = 1000;}
      void InitializeOrder(int numPizzas);
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const;
      int ComputeTotal() const ;
  };

  class Pizzeria {
    public:
      Order LastOrder;
      map<string, Ingredient> IngredientMap;
      unordered_map<string, Pizza> PizzaSet;
      unordered_map<int, Order> OrderSet;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);
      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const ;
      string GetReceipt(const int& numOrder) const ;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
